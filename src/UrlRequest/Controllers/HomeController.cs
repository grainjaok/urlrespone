﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using UrlRequest.Models;
using Newtonsoft.Json;


namespace UrlRequest.Controllers
{


	public class HomeController : Controller
	{
		private ProgramContext _context;
		public HomeController(ProgramContext context)
		{
			_context = context;			
			
		}


		[HttpGet]
		public IActionResult Index()
		{

			
			ViewBag.JsonList = JsonConvert.SerializeObject(_context.Urls.ToList());

			return View();
		}


		public JsonResult AddUrl([FromBody] UrlJson data)
		{
			try
			{
				TimeHelper timeHelper = new TimeHelper(data.Value);
				var urlToAdd = new Url();
				urlToAdd.UrlString = data.Value;
				urlToAdd.DateModified = DateTime.Now;
				urlToAdd.Pending = timeHelper.pending;
				_context.Urls.Add(urlToAdd);
				_context.SaveChanges();
			
			}
			catch (Exception)
			{

				//throw; 
			}

			return Json(_context.Urls.ToList());
		}


	}
}
