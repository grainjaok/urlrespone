﻿
var app = app || angular.module('app', ["chart.js"]);

app.controller("UrlController", function ($scope, $http, $window) {
    
    $scope.init = function (addUrl, listUrl) {
        $scope.addNewUrl = addUrl;
        $scope.jsonListUrls = listUrl;
        

        var arrData = new Array();
        var arrLabels = new Array();
        $.map($scope.jsonListUrls, function (item) {
            arrData.push(item.Pending);
            arrLabels.push(item.UrlString);
        })
        $scope.data = [];
        $scope.labels = [];
        $scope.data.push(arrData);

        for (var i = 0; i < arrLabels.length; i++) {
            $scope.labels.push(arrLabels[i]);
        }
    };
    

    
    $scope.sendUrl = function () {

        var stringData = JSON.stringify({ "value": $scope.name });
        $http.post($scope.addNewUrl, stringData, [({ 'Content-Type': 'application/json' })]).success(function (data) {
            $scope.jsonListUrls = data;
            $window.location.reload();
        })
    };
})