﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace UrlRequest.Models
{
	public class TimeHelper
	{
		private static string _url;
		public int pending { get; set; }
		public TimeHelper(string url)
		{
			_url = url;
			var inforesult =  _HttpServerDemo(_url);
			pending = inforesult.Result.Item2.Milliseconds;
			
			

		}
		private async Task<Tuple<HttpResponseMessage, TimeSpan>> _HttpServerDemo(string url)
		{
			var info = _GetHttpWithTimingInfo(url);
			await Task.WhenAll(info);			
			return info.Result;


		}

		public async Task<Tuple<HttpResponseMessage, TimeSpan>> _GetHttpWithTimingInfo(string url)
		{
			var stopWatch = Stopwatch.StartNew();
			using (var client = new HttpClient())
			{
				var result = await client.GetAsync(url);
				return new Tuple<HttpResponseMessage, TimeSpan>(result, stopWatch.Elapsed);
			}
		}


	}
}
