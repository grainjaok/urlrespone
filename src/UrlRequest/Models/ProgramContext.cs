﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UrlRequest.Models
{
    public class ProgramContext : DbContext
	{
		public ProgramContext(DbContextOptions<ProgramContext> options) :base(options)
        { }
		public DbSet<Url> Urls { get; set; }
	}
}
