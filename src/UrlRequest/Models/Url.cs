﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UrlRequest.Models
{
	public class Url
	{

		public int Id { get; set; }

		public string UrlString { get; set; }

		public int Pending { get; set; }

		public DateTime DateModified { get; set; }

	}

	public class UrlJson
	{
		public string Value { get; set; }
	}

}
