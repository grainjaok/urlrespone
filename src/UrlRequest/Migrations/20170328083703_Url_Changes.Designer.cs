﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using UrlRequest.Models;

namespace UrlRequest.Migrations
{
    [DbContext(typeof(ProgramContext))]
    [Migration("20170328083703_Url_Changes")]
    partial class Url_Changes
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.0.1");

            modelBuilder.Entity("UrlRequest.Models.Url", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("DateModified");

                    b.Property<DateTime>("Pending");

                    b.Property<string>("UrlString");

                    b.HasKey("Id");

                    b.ToTable("Urls");
                });
        }
    }
}
